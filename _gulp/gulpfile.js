/*
# install: 
  - node.js (https://nodejs.org/uk/)
  - ruby-installer (https://rubyinstaller.org/downloads)

# use:
  $ cd gulp
  $ npm install
  $ gem install compass
  $ compass install compass --sass-dir "sass" --css-dir "css" --javascripts-dir "js" --images-dir "images"
  $ gulp watch --url travel --min
  --
  $ gulp imagemin --url travel --min
  $ gulp svg --url travel

# delete node_modules
  $ npm install rimraf -g
  $ rimraf node_modules
*/

let gulp = require('gulp'),
argv = require('yargs').argv,
gulpif = require('gulp-if'),
compass = require('gulp-compass'),
path = require('path'),
// sass = require('gulp-sass'),
autoprefixer = require('gulp-autoprefixer');
concat = require('gulp-concat'),
uglify = require('gulp-uglify'),
cleanCSS = require('gulp-clean-css');
imagemin = require('gulp-imagemin'),
pngquant = require('imagemin-pngquant'),
uncss = require('gulp-uncss'),
rename = require('gulp-rename'),
watch = require('gulp-watch');
replace = require('gulp-replace');
cheerio = require('gulp-cheerio');
svgSprite = require('gulp-svg-sprite');
svgmin = require('gulp-svgmin');

var url = '../' + argv.url + '/';

// task  default
gulp.task('default', () => {
  gulp.src(url + 'sass/*.scss')
    .pipe(compass({
      project: path.join(__dirname, url),
      css: 'css',
      sass: 'sass',
      image: 'images'
    }))
    .on('error', function(error) {
      console.log('gulpError -- ' + error);
      this.emit('end');
    })
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))
    .pipe(concat('all.css'))
    .pipe(gulp.dest(url+'build/unminify'))
    .pipe(gulpif(argv.min, cleanCSS()))
    .pipe(gulpif(argv.min, rename({
        suffix: '.min'
    })))
    .pipe(gulpif(argv.min, gulp.dest(url + 'build/')));

  gulp.src(url + './js/*.js')
    .pipe(concat('all.js'))
    .pipe(gulp.dest(url + 'build/unminify'))
    .pipe(gulpif(argv.min, uglify()))
    .pipe(gulpif(argv.min, rename({ suffix: '.min' })))
    .pipe(gulpif(argv.min, gulp.dest(url + 'build/')));
});

// task imagemin
gulp.task('imagemin', () => {
  return gulp.src(url + './images/*')
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{
        removeViewBox: false
      }],
      use: [pngquant()]
    }))
    // .pipe(gulp.dest(url + 'build/images'));
    .pipe(gulpif(argv.min, gulp.dest(url + 'build/images/')));
});

gulp.task('svg', () => {
  return gulp.src(url + './images/svg/**/*')
    .pipe(svgmin({
      js2svg: {
        pretty: true
      }
    }))
    .pipe(cheerio({
      run: function ($) {
        $('[fill]').removeAttr('fill');
        $('[stroke]').removeAttr('fill');
        $('[style]').removeAttr('fill');
      },
      parserOptions: {
        xmlMode: true
      }
    }))
    .pipe(replace('&gt;', '>'))
    .pipe(svgSprite({
      mode: {
        symbol: {
          sprite: 'sprite.svg'
        }
      }
    }))
    .pipe(gulp.dest(url + 'build/images/svg/'));
  });

// task watch
gulp.task('watch', () => {
  gulp.watch(url + './sass/*.scss', ['default'])
  gulp.watch(url + './js/*.js', ['default'])
  return gulp;
});
